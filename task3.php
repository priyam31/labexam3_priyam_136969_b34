<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>A Chess Board (8X8) by using CSS (not table).</title>
    <style>
        *{margin:0;padding:0;}
        h1{text-align:center;}
        .main{width:98%;margin:10% auto;border: #7be9ff}
        .box{width:150px;height: 150px;background:green;text-align:center;display:inline-block;}
        .box span{display:block; padding-top:50%;margin-top:-9px; font-size:18px;}
        .box:(2n)th-child{background:#fff;}
    </style>
</head>
<body>
<h1>A Chess Board (8X8) by using CSS (not table).</h1>
<div class="main">
    <?php
    for($i=1;$i<=8;$i++){
        for($j=1;$j<=8;$j++){

            if($i&1){

                if($j&1){
                    echo '<div style="background:#000;" class="box"><span></span></div>';
                }
                else{
                    echo '<div style="background:pink;" class="box"><span></span></div>';

                }

            } else{

                if($j&1){
                    echo '<div style="background:pink;" class="box"><span></span></div>';
                }
                else{
                    echo '<div style="background:#000;" class="box"><span></span></div>';

                }


            }
        }


    }
    ?>
</div>
</body>
</html>